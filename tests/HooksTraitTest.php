<?php
Namespace dgifford\Traits;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class Foo
{
	Use HooksTrait;

	public function output( $value = '' )
	{
		// A hook with only a subject
		$value = $this->doHook('before', $value );

		$value .= '[Added by Output]';

		// A hook which provides additional arguments
		$value = $this->doHook('after', $value, 'WooWar' );

		return $value;
	}


	public function uppercase( $value )
	{
		return strtoupper( $value );
	}
}



class Bar
{
	Use HooksTrait;

	public $num = 0;

	public function add()
	{
		$this->num++;

		// A hook that gets sent the whole object
		$this->doHook('after_plus_one', $this );
	}
}



class HooksTraitTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->foo = New Foo;
		$this->bar = New Bar;
	}



	public function testNoHooksDefined()
	{
		$this->assertSame( 'Yo[Added by Output]', $this->foo->output( 'Yo' ) );
	}



	public function testAddClosureBeforeHook()
	{
		$this->foo->addHook( 'before', function( $value ) { return 'BEFORE ' . $value; } );

		$this->assertSame( 'BEFORE Yo[Added by Output]', $this->foo->output( 'Yo' ) );
	}



	public function testAddObjectAndMethodBeforeHook()
	{
		$this->foo->addHook( 'before', [ $this->foo, 'uppercase' ] );

		$this->assertSame( 'YO[Added by Output]', $this->foo->output( 'Yo' ) );
	}



	public function testAddUncallableHook()
	{
		$this->foo->addHook( 'before', [ $this->foo, 'wibble' ] );

		$this->assertSame( 'Yo[Added by Output]', $this->foo->output( 'Yo' ) );
	}



	public function testAddBeforeAndAfterHooks()
	{
		$this->foo->addHook( 'before', [ $this->foo, 'uppercase' ] );

		$this->foo->addHook( 'after', function( $value ) { return "$value FOO"; } );

		$this->assertSame( 'YO[Added by Output] FOO', $this->foo->output( 'Yo' ) );
	}



	public function testAddHooksWithPriorities()
	{
		$this->foo->addHook( 'after', function( $value ) { return "$value BAR"; }, 20 );

		$this->foo->addHook( 'after', function( $value ) { return "$value FOO"; } );

		$this->assertSame( 'Yo[Added by Output] FOO BAR', $this->foo->output( 'Yo' ) );
	}



	public function testAdditionalArgumentsPassedToHookUsed()
	{
		$this->foo->addHook( 'after', function( $value, $arg1 ) { return "$value FOO $arg1"; } );

		$this->assertSame( 'Yo[Added by Output] FOO WooWar', $this->foo->output( 'Yo' ) );
	}



	public function testApplyHookToAnObject()
	{
		$this->bar->add();

		$this->assertSame( 1, $this->bar->num );

		$this->bar->addHook( 'after_plus_one', function( $obj ) { $obj->num = $obj->num + 5; } );

		$this->assertSame( 1, $this->bar->num );

		$this->bar->add();

		$this->assertSame( 7, $this->bar->num );
	}



}