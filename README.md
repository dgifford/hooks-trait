# Hooks Trait #

Adds the methods required to use hooks in a class, similar to how functions can be hooked into Wordpress.

## Usage ##

Add a ```use``` statement at the start of the class to include the trait.

```
class Foo
{
	Use dgifford\Traits\HooksTrait;
}
```

The ```doHook``` method adds a hook into the class. This method requires a name for the hook and a variable that the hook will act on.

```
$value = $this->doHook('my_hook_name', $value );

```

Additional arguments can be provided if hooked in functions require them.
```
$value = $this->doHook('my_hook_name', $value, $arguments );

```

Callbacks can then be hooked into the class with the ```addHook``` method, which accepts the hook name and a callable, such as a closure.
```
$foo->addHook( 'my_hook_name', function( $value, $arguments ) { return $value + 1; } );
```
