<?php
Namespace dgifford\Traits;

/*
	Adds Wordpress-like hook functionality to a class.

	The class can define hooks (or events) within it's code using doHook 
	and callbacks can be assigned to the hook using addHook.

    Copyright (C) 2017  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
trait HooksTrait
{
	public $hooks = [];



	/**
	 * Add a callable to the list of operations for a hook.
	 * 
	 * @param string  	$hook      Hook name
	 * @param callable 	$callable 
	 * @param integer 	$priority
	 */
	public function addHook( $hook, $callable, $priority = 10, $alias = '' )
	{
		if( !is_callable($callable) )
		{
			return;
		}

		if( !array_key_exists( $hook, $this->hooks ) )
		{
			$this->hooks[ $hook ] = [];
		}

		$this->hooks[ $hook ][] = 
		[
			'callable' 	=> $callable, 
			'priority' 	=> $priority,
			'alias'		=> $alias,
		];
	}



	/**
	 * Define a hook, provide the argument(s) that callbacks 
	 * can access and call hooked in callbacks.
	 * 
	 * @param string  $hook      	Hook name
	 * @param  mixed  $subject 		Subject of the hook, the variable that will be altered
	 * @param  mixed  $arguments 	Additional arguments passed to callbacks
	 * @return mixed  $subject 		Modified by the callbacks
	 */
	public function doHook( $hook = '', $subject = '', $arguments = [] )
	{
		if( is_string($hook) and !empty($hook) and isset($this->hooks[ $hook ]) and !empty($this->hooks[ $hook ]) )
		{
			// Sort hooks by priority
			usort( $this->hooks[ $hook ], function($a, $b)
			{
				return $a['priority'] - $b['priority'];
			});

			// Call the callback for each hook
			foreach( $this->getHooks( $hook ) as $arr )
			{
				if( is_object($subject) )
				{
					call_user_func_array( $arr['callable'], [$subject, $arguments] );
				}
				else
				{
					$subject = call_user_func_array( $arr['callable'], [$subject, $arguments] );
				}
			}
		}

		// Don't return an object - changed by reference
		if( !is_object( $subject ) )
		{
			return $subject;
		}		
	}



	/**
	 * Clear all the functions associated with a hook.
	 * @param  string $hook
	 * @return null
	 */
	public function getHooks( $hook = '' )
	{
		if( isset( $this->hooks[ $hook ] ) )
		{
			return $this->hooks[ $hook ];
		}
	}



	/**
	 * Clear all the functions associated with a hook.
	 * @param  string $hook
	 * @return null
	 */
	public function clearHook( $hook = '' )
	{
		if( isset( $this->hooks[ $hook ] ) )
		{
			unset($this->hooks[ $hook ]);
		}
	}
}